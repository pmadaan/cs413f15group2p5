package uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.test;

import android.annotation.TargetApi;
import android.os.Build;

import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.MainActivity;


import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.test.ui.AbstractGameTest;

/**
 * Created by Shoroog on 12/6/15.
 */
public class GameActivityTest extends android.test.ActivityInstrumentationTestCase2<MainActivity> {

    @TargetApi(Build.VERSION_CODES.FROYO)
    public  GameActivityTest() {
        super(MainActivity.class);
        actualTest = new AbstractGameTest() {
            @Override
            protected MainActivity getActivity() {
                // return activity instance provided by instrumentation test
                return  GameActivityTest.this.getActivity();
            }
        };

    }

    private AbstractGameTest actualTest;

    public void testMonsterNumber(){
        actualTest.countTheMonsters();
    }

    public void testActivityCheckTestCaseSetUpProperly() {
        actualTest.testActivityCheckTestCaseSetUpProperly();
    }
}
