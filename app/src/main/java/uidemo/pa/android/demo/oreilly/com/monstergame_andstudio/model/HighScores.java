package uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by pmadaan on 12/7/2015.
 */
public class HighScores {
    // save current score to highscores.
    public static void saveScore(Context context, int score){
        // get current high scores
        int highScores[] = HighScores.getHighScores(context);

        /* if current socre can be high score, then put it in appropriate postion. */
        for(int i=0;i<5; i++){
            if(score > highScores[i]){
                for(int j=i+1; j<5; j++){
                    highScores[j] = highScores[j-1];
                }
                highScores[i] = score;
                break;
            }

        }
        // save new high scores to shared preference of android
        SharedPreferences.Editor editor = context.getSharedPreferences(G.tag_HighScore_Pref, 0).edit();
        for(int i=0;i<5; i++){
            editor.putInt(G.tag_HighScores[i], highScores[i]);
        }
       editor.commit();
    }
    // get high scores. count is 5.
    public static int[] getHighScores(Context context){
        int highScores[] = new int[5];
        SharedPreferences sharedpreferences = context.getSharedPreferences(G.tag_HighScore_Pref, 0);
        for(int i=0;i<5; i++){
            highScores[i] = sharedpreferences.getInt(G.tag_HighScores[i], 0);
        }
        return  highScores;
    }
}
