package uidemo.pa.android.demo.oreilly.com.monstergame_andstudio;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.hardware.SensorManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model.G;
import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model.HighScores;
import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model.Monster;
import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model.Monsters;
import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.view.MonsterGridAdapterView;

public class MainActivity extends AppCompatActivity {

    GridView mainView;  //grid view of game panel
    MonsterGridAdapterView adapterView; //view class which links monsters data to gridview
    Monsters monstersModel = new Monsters(); // model which saves all mosters data
    MonsterGenerator monsterGenerator; //thread which controlls monsters

    TextView txtScore, txtTime; //widgets which show score, remaining time
    Button btn_play; // play/pause button

    boolean dataInited; // flag which represents whether monster data is initiated according to screen panel size.
    boolean gamePaused; // flag which represents whether game is paused or playing.

    boolean gameEnded = false; //flag which represents whether game is ended or not.

    int score; //current score
    int timeRemaining; //remaining time

    private final Random rand = new Random();
    private int mCurrentOrientation; //screen orientation
    private int mCurrentOrientationType; //screen rotation type. it has 4 types. 0 is normal portrait, increased one by one according to 90 degress.
    private OrientationEventListener orientationEventListener; //listener which represents screen rotation

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set game screen in full screen mode
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // get instances of widgets from resources
        txtScore = (TextView)findViewById(R.id.txt_Score);
        txtTime = (TextView)findViewById(R.id.txt_TimeElapsed);

        //grid view which is the main game paenl
        mainView = (GridView)findViewById(R.id.mainView);
        // set choice mode in single mode so that user can killed only one monster at once.
        mainView.setChoiceMode(GridView.CHOICE_MODE_SINGLE);

        //layout which has main game grid view as child. we set M,N here according to this layout's size
        final RelativeLayout layout = (RelativeLayout)findViewById(R.id.layout_main);
        layout.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        // if game data is not initiated then, we set M and N then initialize the monster data with M and N
                        if (!dataInited) {
                            int m_Width = layout.getWidth();
                            int m_Height = layout.getHeight();

                            G.M = m_Width / G.cellSize;
                            G.N = m_Height / G.cellSize - 1;
                            mainView.setNumColumns(G.M);

                            initMonsterData();

                        }

                    }
                }
        );

        // if user clicks on grid cell, we must process monster on that position
        mainView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // if game is paused, then this operation ignores
                if (gamePaused) {
                    return;
                }
                // get monster in that clicked position
                Monster monster = (Monster) mainView.getAdapter().getItem(position);

                //if monster is killed, then update score and shows it in UI
                if (monstersModel.monsterKilled(monster)) {
                    score++;
                    txtScore.setText("Score: " + score);
                }

            }
        });

        // get instance of play button
        btn_play = ((Button) findViewById(R.id.btn_play));
        btn_play.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // if game ended, then shows high score list
                        if(gameEnded){
                            showHighScores();
                            return;
                        }
                        // if game paused, resume game
                        if (gamePaused) {
                            startGame();
                        } else {
                            // if game is running, then pause game
                            pauseGame();

                        }

                    }
                });
        // get instance of exit button.
        ((Button) findViewById(R.id.btn_exit)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.exit(0);
                    }
                }
        );
        // get instance of reset button and reset game data on clicking event.
        ((Button) findViewById(R.id.btn_reset)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        resetGame();
                    }
                }
        );
        // initialize view with monster data
        adapterView =  new MonsterGridAdapterView(this, R.layout.monster, monstersModel);
        mainView.setAdapter(adapterView);

        // set interface of monsters' change listener.
        monstersModel.setMonstersChangeListener(new Monsters.MonstersChangeListener() {

            // here we override the main process for  changing event.
            @Override
            public void onMontersChange(Monsters monsters) {
                adapterView.notifyDataSetChanged();
            }
        });

        // spinner widget for setting level
        final Spinner level_spinner = (Spinner)findViewById(R.id.spinner_level);

        // reset the monster data according to the level.
        level_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String str_level = level_spinner.getSelectedItem().toString();
                if (str_level.equalsIgnoreCase("1")) {
                    G.level = 1;
                } else if (str_level.equalsIgnoreCase("2")) {
                    G.level = 2;
                } else if (str_level.equalsIgnoreCase("3")) {
                    G.level = 3;
                } else if (str_level.equalsIgnoreCase("4")) {
                    G.level = 4;
                } else if (str_level.equalsIgnoreCase("5")) {
                    G.level = 5;
                } else {
                    G.level = 1;
                }
                resetGame();
                //monsterGenerator.setMonsters(monstersModel);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mCurrentOrientation = getInitialScreenOrientation();

        // currentScreenOrientation type. It is value of 0~3.
        mCurrentOrientationType = 0;

        // if currentScreenOrientationType is changed, then we update monsters' image.
        orientationEventListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int orientation) {

                int newScreenOrientationType = getScreenOrientationType(orientation);
                if(newScreenOrientationType != mCurrentOrientationType){
                    monstersModel.setImageTypes(newScreenOrientationType);
                    mCurrentOrientationType = newScreenOrientationType;
                }

            }
        };

        //enable screen rotation
        orientationEventListener.enable();

        // initialize game information
        initGame();

    }
    //method which get initial screen orientation
    private int getInitialScreenOrientation(){
        Display display = getWindowManager().getDefaultDisplay();
        int orientation = display.getOrientation();
        if (orientation == Configuration.ORIENTATION_UNDEFINED) {
            orientation = getResources().getConfiguration().orientation;

            if(orientation == Configuration.ORIENTATION_UNDEFINED){
                if(display.getWidth() == display.getHeight()){
                    orientation = Configuration.ORIENTATION_SQUARE;
                }else if(display.getWidth() < display.getHeight()){
                    orientation = Configuration.ORIENTATION_PORTRAIT;
                }else{
                    orientation = Configuration.ORIENTATION_LANDSCAPE;
                }
            }

        }
        return orientation;
    }
    //method which get screen orientation. it returns portrait or landscape.
    private int getScreenOrientation(int orientationInDegrees){
        int newOrientation = mCurrentOrientation;
        if(orientationInDegrees != OrientationEventListener.ORIENTATION_UNKNOWN){
            if(orientationInDegrees < 270 + G.ROTATION_THRESHOLD && orientationInDegrees > 270 - G.ROTATION_THRESHOLD){
                newOrientation = Configuration.ORIENTATION_LANDSCAPE;
            }else if(orientationInDegrees < G.ROTATION_THRESHOLD || orientationInDegrees > 360 - G.ROTATION_THRESHOLD){
                newOrientation = Configuration.ORIENTATION_PORTRAIT;
            }
        }
        return  newOrientation;
    }
    // method for getting current screen rotation type from screen angle. it has values of 0~3
    private int getScreenOrientationType(int orientationInDegrees){
        int newOrientationType = mCurrentOrientationType;
        if(orientationInDegrees != OrientationEventListener.ORIENTATION_UNKNOWN){
            if(orientationInDegrees < G.ROTATION_THRESHOLD || orientationInDegrees > 360 - G.ROTATION_THRESHOLD){
                newOrientationType = 0;
            }else if(orientationInDegrees <= 90 + G.ROTATION_THRESHOLD && orientationInDegrees >=  G.ROTATION_THRESHOLD){
                newOrientationType = 3;
            }else if(orientationInDegrees < 180 + G.ROTATION_THRESHOLD && orientationInDegrees > 90 +  G.ROTATION_THRESHOLD){
                newOrientationType = 2;
            }else {
                newOrientationType = 1;
            }
        }
        return  newOrientationType;
    }
    //if game ended, we must update highscores with current scsore
    private void saveHighScore(){
        HighScores.saveScore(this, score);
    }
    // show dialog which shows the list of highscores
    private void showHighScores(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.highscores, null);
        builder.setView(dialogView);
        builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        builder.setTitle("High Scores");
        AlertDialog highscore_dialog;
        highscore_dialog = builder.create();
        highscore_dialog.show();

        int highScores[] = HighScores.getHighScores(this);
        TextView txt_HighScore1 = (TextView)dialogView.findViewById(R.id.txt_highScore1);
        txt_HighScore1.setText(String.valueOf(highScores[0]));
        TextView txt_HighScore2 = (TextView)dialogView.findViewById(R.id.txt_highScore2);
        txt_HighScore2.setText(String.valueOf(highScores[1]));
        TextView txt_HighScore3 = (TextView)dialogView.findViewById(R.id.txt_highScore3);
        txt_HighScore3.setText(String.valueOf(highScores[2]));
        TextView txt_HighScore4 = (TextView)dialogView.findViewById(R.id.txt_highScore4);
        txt_HighScore4.setText(String.valueOf(highScores[3]));
        TextView txt_HighScore5 = (TextView) dialogView.findViewById(R.id.txt_highScore5);
        txt_HighScore5.setText(String.valueOf(highScores[4]));

    }
    //init game data
    public void initGame(){
        //level is 1 in initial.
        if(G.level == 0){
            G.level = 1;
        }
        // score is 0 and shows it
        score = 0;
        txtScore.setText("Score: " + score);

        // set remainig time to initial values and shows it in UI
        timeRemaining = G.initTime;
        txtTime.setText("Time: " + (timeRemaining/10.0f) + "s");

        // game is puased and data is not inited in app startup
        gamePaused = true;
        dataInited = false;

    }

    // reset game data
    public void resetGame(){
        pauseGame();

        initGame();

        //clear current moster data
        monstersModel.clearMonsters();
        //create new monster data
        initMonsterData();

        //make play button enable after resetting monster data
        btn_play.setEnabled(true);

        if(gameEnded){
            // if game ended, thread for monsters controlling muest be set to null
            monsterGenerator = null;
        }
        // game is not started, so gameended is null
        gameEnded = false;
    }

    //start game
    public void startGame(){
        // set game pause flag to false
        gamePaused = false;

        // if monster generator thread is not instantiated,then we must intialize it and then start it
        if(monsterGenerator == null){
            monsterGenerator = new MonsterGenerator(monstersModel);
            new Thread(monsterGenerator).start();
        }else{
            monsterGenerator.setPauseStatus(gamePaused);
        }
        btn_play.setText("PAUSE");
        btn_play.setTextColor(Color.rgb(0, 255, 0));

    }
    // gaem is paused
    public void pauseGame(){
        gamePaused = true;
        // if monster generator thread is not instantiated,then we must intialize it and then pause it
        if(monsterGenerator == null){
            monsterGenerator = new MonsterGenerator(monstersModel);
            monsterGenerator.setPauseStatus(gamePaused);
            new Thread(monsterGenerator).start();
        }else{
            monsterGenerator.setPauseStatus(gamePaused);
        }
        btn_play.setText("PLAY");
        btn_play.setTextColor(Color.rgb(255, 0, 0));
    }
    //initialize monster data
    public void initMonsterData(){
        //set dataInited flag to true
        dataInited = true;

        //level is not selected then set it to 1.
        if(G.level < 1){
            G.level = 1;
        }

        //set monsters count K according to M, N and monster distribution value
        G.K = (int) (G.M * G.N * G.monsterDistribution[G.level-1]);

        ArrayList<Integer> available_list = new ArrayList<Integer>();
        for(int i=0; i<G.K; i++){

            //create k monsters randomly
            int monster_index = (rand.nextInt()%(G.M * G.N) + (G.M * G.N))%(G.M * G.N);
            while (available_list.contains(monster_index)){
                monster_index = (rand.nextInt()%(G.M * G.N) + (G.M * G.N))%(G.M * G.N);
            }
            //add created monster to monster list
            available_list.add(monster_index);
        }

        // create other emtpy cells in grid view
        Monster monster;
        for(int i=0; i<G.M * G.N; i++){
            if(available_list.contains(i)){
                if(i % 2 == 0){
                    monster = new Monster(false, true);
                }else{
                    monster = new Monster(true, true);
                }
            }else{
                monster = new Monster(true, false);
            }
            monstersModel.addMonster(monster);
        }

     }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //game is ended
    public void gameOver(){

        // finish thread by setting done flag to true
        monsterGenerator.done();

        btn_play.setText("Show High Scores");
        btn_play.setTextColor(Color.rgb(0, 255, 0));
        gamePaused = true;
        gameEnded = true;

        // store high score
        saveHighScore();
        txtTime.setText("Game Over");
    }

    // update remaining time
    public void updateTime()
    {
        timeRemaining --;

        // if remaining time is less than 0, then ends the game
        if(timeRemaining <= 0){
            gameOver();
        }
        txtTime.setText("Time: " + (timeRemaining / 10.0f) + "s");
    }

    // update monsters
    public void updateMonsters(Monsters monsters){
        monsters.updateMonsters();
    }
    public Point getScreenSize(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }
    @Override
    public void onPause() {
        super.onPause();

        orientationEventListener.disable();
        if(gameEnded){

        }else {
            pauseGame();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        orientationEventListener.enable();
    }

    // thread for controlling monsters
    private final class MonsterGenerator implements Runnable {

        private volatile boolean done; // flag which represents whether game is done or not
        private volatile boolean paused; //flag which represents whether game is paused or running

        Monsters monsters; //address of monsters data

        private final Handler hdlr = new Handler(); //handler which process monsters in one time
        private final Runnable changeMonsterState = new Runnable() {
            @Override public void run() {
                // update remaining time
                updateTime();

                // change monsters' status
                updateMonsters(monsters);
            }
        };

        //constructor
        MonsterGenerator(Monsters monsters) {
            this.monsters = monsters;
            this.paused = false;
            this.done = false;
        }
        // set monsters
        public void setMonsters(Monsters monsters){
            this.monsters = monsters;
        }
        // game is over
        public void done() {
            done = true;
        }
        // set game ended flag
        public void setDone(boolean value){
            done = value;
        }
        // set game paused flag
        public void setPauseStatus(boolean paused) {
            this.paused = paused;
        }

        @Override
        public void run() {
            while (!done) {
                // if game is paused then ignore, otherwise create handler which precesses monster data
                if(!paused) {
                    hdlr.post(changeMonsterState);
                }
                //run this action once inn 100 milli seconds
                try {
                    Thread.sleep(100);
                }catch (InterruptedException e) {

                }
            }
        }

    }

}
