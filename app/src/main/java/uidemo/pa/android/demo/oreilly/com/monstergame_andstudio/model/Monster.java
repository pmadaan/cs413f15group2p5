package uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model;

import java.util.Random;

/**
 * Created by pmadaan on 12/5/2015.
 */
public class Monster {

    boolean protectStatus;
    boolean alive;

    int moveTickCount; // flag which represents whether monster is in vulnerable
    int statusTickCount; // flag which represents whether monster is kiilled(or hidden) or visible
    int vulnerablePercent; //number which represents the remaining tick count til the monster moves to adjacent point
    int imageType; // number which represents the remaining tick count til the monster changes its status from protected to vulnerable or from vulnerable to protected.

    private final Random rand = new Random();

    public Monster(){
        protectStatus = false;
        alive = true;
        this.imageType = 0;
    }
    public Monster(boolean protectStatus, boolean alive){
        this.protectStatus = protectStatus;
        this.alive = alive;
        this.moveTickCount = G.default_moveTickCount_min;
        this.imageType = 0;

        this.vulnerablePercent = G.default_vulnerablePercent_min[G.level-1];

        /* set the statusTickCount randomly wihtin vulnerablepercent range  */
        if(protectStatus){
            /* if monster is in protected state, it has larger values than vulnerablePercent. it is up to 100. */
            int minMaxDiff = 100 - G.default_vulnerablePercent_max[G.level-1];
            int offset = (rand.nextInt()%minMaxDiff + minMaxDiff)%minMaxDiff;

            this.statusTickCount = G.default_vulnerablePercent_max[G.level-1] + offset;
        }else {
            /* if monster is in vulnerable state, it has less values than vulnerablePercent. its min value is 0. */
            int minMaxDiff = G.default_vulnerablePercent_max[G.level-1];
            int offset = (rand.nextInt()%minMaxDiff + minMaxDiff)%minMaxDiff;

            this.statusTickCount = this.vulnerablePercent;
        }
     }
    public Monster(boolean protectStatus, boolean alive, int moveTickCount, int vulnerablePercent){
        this.protectStatus = protectStatus;
        this.alive = alive;
        this.moveTickCount = moveTickCount;
        this.vulnerablePercent = vulnerablePercent;
        if(protectStatus){
            int minMaxDiff = 100 - G.default_vulnerablePercent_max[G.level-1];
            int offset = (rand.nextInt()%minMaxDiff + minMaxDiff)%minMaxDiff;

            this.statusTickCount = G.default_vulnerablePercent_max[G.level-1] + offset;
        }else {
            int minMaxDiff = G.default_vulnerablePercent_max[G.level-1];
            int offset = (rand.nextInt()%minMaxDiff + minMaxDiff)%minMaxDiff;

            this.statusTickCount = this.vulnerablePercent;
        }
        this.imageType = 0;
    }
    public void setAlive(boolean alive){
        this.alive = alive;
    }
    public boolean isAlive(){
        return alive;
    }
    public void setProtectStatus(boolean protectStatus){
        this.protectStatus = protectStatus;
    }
    public boolean isProtected(){
        return protectStatus;
    }
    public void setMoveTickCount(int moveTickCount){
        this.moveTickCount = moveTickCount;
    }
    public int getMoveTickCount(){
        return moveTickCount;
    }
    public boolean isMovable(){
        if(moveTickCount < 0){
            return  true;
        }
        return false;
    }
    public void setVulnerablePercent(int vulnerablePercent){
        this.vulnerablePercent = vulnerablePercent;
    }
    public int getVulnerablePercent(){
        return vulnerablePercent;
    }
    public int getStatusTickCount(){
        return statusTickCount;
    }
    public void setStatusTickCount(){
        this.statusTickCount = statusTickCount;
    }
    public boolean isStatusChangeable(){
        if(statusTickCount == vulnerablePercent){
            return true;
        }
        if(statusTickCount <= 0){
            return true;
        }

        return false;
    }
    public void changeStatus(){
        /* if statusTickCount reaches 0 then it changes into protected status*/
        if(statusTickCount <= 0){
            this.statusTickCount = 100;
            this.protectStatus = true;
            return;
        }
         /* if statusTickCount reaches vulnerable state then it changes into vulnerable status*/
        if(statusTickCount <= vulnerablePercent){
            this.protectStatus = false;
        }

    }
    public void moved(int moveTickCount, boolean alive){
        this.alive = alive;
        this.moveTickCount = moveTickCount;
    }
    public void updateTickCounts(){
        this.statusTickCount--;
        this.moveTickCount--;
    }
    public void setimageType(int type){
        this.imageType = type;
    }
    public int getImageType()
    {
        return  this.imageType;
    }
}
