package uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model;

import java.util.Random;

import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.R;

/**
 * Created by pmadaan on 12/6/2015.

 This is a class which sets constants for game
 */

public class G {

    public static final int default_moveTickCount_min = 20;   //min value of monster's move tick count
    public static final int default_moveTickCount_max = 50;  //max value of monster's move tick count

    public static final int[] default_vulnerablePercent_max = new int[] {50, 45, 40, 35, 30}; // min value of monster's vulnerable state in each level
    public static final int[] default_vulnerablePercent_min = new int[] {40, 35, 30, 25, 20}; // max value of monster's vulnerable state in each level

    public static int level = 0;  //selected level. it has number of 1~5.

    public static int M; //number of gridview's columns in portrait standard screen mode.
    public static int N; //number of gridview's rows in portrait standard screen mode.
    public static int K; //number of monsters
    public static final int cellSize = 60; //size of gridview cell for monster
    public static final int initTime = 600; //game duration

    public static final float[] monsterDistribution = new float[]{0.15f, 0.2f, 0.25f, 0.3f, 0.35f}; //monsters' distribution according to levels

    public static final int[][] adjacentList = new int[][]{
            {-1, -1}, {-1, 0}, {-1, 1}, {0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}
    }; //arraay which represents adjacent points.

    public static final int ROTATION_THRESHOLD = 45;  //threshold value of screen rotation from normal state.

    public static final int[] monster_protected_id = new int[]{R.drawable.mon1_protected_0, R.drawable.mon1_protected_1, R.drawable.mon1_protected_2, R.drawable.mon1_protected_3};
   //resources for monsters in protected status according to each screen rotation.

    public static final int[] monster_normal_id = new int[]{R.drawable.mon1_normal_0, R.drawable.mon1_normal_1, R.drawable.mon1_normal_2, R.drawable.mon1_normal_3};
    //resources for monsters in vulnerable status according to each screen rotation.

    // constants for storing high score values.
    public static final String[] tag_HighScores=  new String[] {"HighScore1", "HighScore2", "HighScore3", "HighScore4", "HighScore5"};
    public static final String tag_HighScore_Pref = "HighScores";
}
