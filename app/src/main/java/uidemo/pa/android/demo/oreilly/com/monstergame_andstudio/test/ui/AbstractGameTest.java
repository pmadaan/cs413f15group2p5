package uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.test.ui;

import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.MainActivity;
import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model.Monsters;
import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.view.MonsterGridAdapterView;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Shoroog on 12/6/15.
 */
public abstract class AbstractGameTest {


    protected abstract MainActivity getActivity();
    private Monsters MonstersTest = new Monsters();
    private MonsterGridAdapterView monsterView;



    @Test
    public void testActivityCheckTestCaseSetUpProperly() {
        assertNotNull("activity should be launched successfully", getActivity());
    }
    @Test
    public void testActivityExists() {
        assertNotNull(getActivity());
    }
    @Test
    public void countTheMonsters(){
        MonstersTest.countMonsters();
        assertEquals(8, MonstersTest.countMonsters());
    }

    @Test
    public void calculateScore() throws Throwable{

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {


            }
        });
    }
}

