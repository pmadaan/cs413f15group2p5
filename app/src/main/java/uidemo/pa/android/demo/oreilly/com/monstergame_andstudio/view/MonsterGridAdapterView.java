package uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.view;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.content.Context;

import java.util.List;

import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.R;
import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model.G;
import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model.Monster;
import uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model.Monsters;

/**
 * Created by pmadaan on 12/5/2015.
 */
public class MonsterGridAdapterView extends ArrayAdapter<Monster> {

    private Context mContext;
    Monsters monsters; //monsters data

    public MonsterGridAdapterView(Context context, int resource, Monsters monsters){
        super(context, resource, monsters.getMonsters());
        this.mContext = context;
        this.monsters = monsters;
    }
    /* show monster in grid view */
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater mInflater = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Monster monster = getItem(position);

        ImageView monsterImg;
        if (convertView == null) {
            monsterImg = new SquareImageView(mContext);
            monsterImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            monsterImg = (ImageView) convertView;
        }
        /* if monster is visible */
        if(monster.isAlive()){
            /* show monster image according to protected or vulnerable state and screen rotation type */
            if(monster.isProtected()){
                monsterImg.setImageResource(G.monster_protected_id[monster.getImageType()]);
            }else{
                monsterImg.setImageResource(G.monster_normal_id[monster.getImageType()]);
            }
        }else{
            /* if monster is invisiblbe then show only empty block */
            monsterImg.setImageResource(R.drawable.mon_empty);
        }
        return monsterImg;
    }


}
