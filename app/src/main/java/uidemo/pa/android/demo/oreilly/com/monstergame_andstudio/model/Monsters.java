package uidemo.pa.android.demo.oreilly.com.monstergame_andstudio.model;

import android.support.annotation.NonNull;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

/**
 * Created by pmadaan on 12/5/2015.
 */
public class Monsters {

    ArrayList<Monster> list_monsters = new ArrayList<Monster>();  //it is main list which preserves all monster data.
    private MonstersChangeListener monstersChangeListener; //listener which receives all monsters’ state changes

    private final Random rand = new Random();

    int moveTickCount_min, moveTickCount_max; // number which represents the range of monsters’ movetickcount. It makes monsters take movetickcount in that range with random number so that they can’t be moved at once and looks actually.
    int vulnerablePercent_min, vulnerablePercent_max;//number which represents the range of monsters’ vulnerable percent as well as movetickcount range.

    //interface which notices the Monsters' status changed event.
    public interface MonstersChangeListener {
        void onMontersChange(Monsters monsters);
    }

    public Monsters(){
        moveTickCount_min = G.default_moveTickCount_min;
        moveTickCount_max = G.default_moveTickCount_max;

        if(G.level < 1){
            G.level = 1;
        }
        vulnerablePercent_min = G.default_vulnerablePercent_min[G.level-1];
        vulnerablePercent_max = G.default_vulnerablePercent_max[G.level-1];
    }
    // returns all monsters data
    public List<Monster> getMonsters() {
        return list_monsters;
    }
    // set the range of move tick count
    public void setMoveTickCountRange(int moveTickCount_min, int moveTickCount_max){
        this.moveTickCount_max = moveTickCount_max;
        this.moveTickCount_min = moveTickCount_min;
    }
    // set the range of vulnerable percent
    public void setVulnerablePercentRange(int vulnerablePercent_min, int vulnerablePercent_max){
        this.vulnerablePercent_max = vulnerablePercent_max;
        this.vulnerablePercent_min = vulnerablePercent_min;
    }
    // set MonsterChanges listener
    public void setMonstersChangeListener(MonstersChangeListener l) {
        monstersChangeListener = l;
    }

    // method which adds monsters to model
    public void addMonster(Monster monster) {
        list_monsters.add(monster);
        notifyListener();
    }
    //method which kills certain monster.
    public boolean monsterKilled(Monster monster) {
        //if monster is in protected state or is invisible, this operation ignores.
        if (monster.isProtected() || !monster.isAlive()) {
            return false;
        }

        // make monster invisible(killed). and create another alive monster to maintain the monsters' count.
        monster.setAlive(false);
        createAliveMonster();
        notifyListener();
        return true;
    }
    //method for update all monsters' state. It decreases each monosters; status and move tick count and if they reach to the limit then change its status or move to adjacent points.
    public void updateMonsters(){
        for(int i=0; i<list_monsters.size(); i++){
            Monster monster = list_monsters.get(i);

            //if monster is invible, then no need to process it.
            if(!monster.isAlive()){
                continue;
            }

            // decrease status change and movet tick count
            monster.updateTickCounts();

            // if monster is avaiable to change status, then change status
            if(monster.isStatusChangeable()){
                monster.changeStatus();
            }

            // if monter is moveable, then move one of random adjacent points and the resent tick counts
            if(monster.isMovable()) {
                Monster adjacent = getAdjacent(i);
                if(adjacent == null){
                    // if adjacent is null then reset tick count only
                    monster.moved(moveTickCount_min + (rand.nextInt() % (moveTickCount_max - moveTickCount_min)), true);
                }else {
                    // if adjacent is not null ,move to that point , reset tick count and make original position invisible
                    monster.moved(moveTickCount_min + (rand.nextInt() % (moveTickCount_max - moveTickCount_min)), false);
                    adjacent.moved(moveTickCount_min + (rand.nextInt() % (moveTickCount_max - moveTickCount_min)), true);
                }

            }
        }
        notifyListener();
    }
    //method which returns the one of randomly adjacent monster
    private Monster getAdjacent(int index){
        int row = index / G.M;
        int col = index % G.M;


        int result_row, result_col, result_index;

        ArrayList<Integer> available_list = new ArrayList<Integer>();

        for(int i=0; i<G.adjacentList.length; i++){
            result_row = row + G.adjacentList[i][0];
            result_col = col + G.adjacentList[i][1];
            if ((result_col < 0) || (result_col >= G.M)) {
                continue;
            }
            if ((result_row < 0) || (result_row >= G.N)) {
                continue;
            }
            result_index = result_row * G.M + result_col;
            if(result_index >= 0 && result_index < list_monsters.size()){
                Monster monster = list_monsters.get(result_index);
                if(!monster.isAlive()){
                    available_list.add(result_index);
                }
            }
        }
        if(available_list.size() == 0){
            return null;
        }
        result_index = (rand.nextInt()%available_list.size() + available_list.size())%available_list.size();
        return list_monsters.get(available_list.get(result_index));

    }
    //method which creates a new visible monster in case user kills the monster. This is for maintainment of monsers' count K.
    public void createAliveMonster() {
        int result_index = (rand.nextInt()%list_monsters.size() + list_monsters.size())%list_monsters.size();
        Monster monster = list_monsters.get(result_index);
        while(monster.isAlive()){
            result_index = (rand.nextInt()%list_monsters.size() + list_monsters.size())%list_monsters.size();
            monster = list_monsters.get(result_index);
        }
        monster.moved(moveTickCount_min + (rand.nextInt() % (moveTickCount_max - moveTickCount_min)), true);
    }
    //method which notices that monsters' status changed to listener.
    private void notifyListener() {
        if (null != monstersChangeListener) {
            monstersChangeListener.onMontersChange(this);
        }
    }
    // method which cleans all monsters
    public void clearMonsters() {
        list_monsters.clear();
        notifyListener();
    }
    // method which sets image type of monsters according to the screen rotaion
    public void setImageTypes(int type){
        for(int i=0; i<list_monsters.size(); i++) {
            Monster monster = list_monsters.get(i);
            monster.setimageType(type);
        }
        notifyListener();
    }
    public int countMonsters() {
        return list_monsters.size();
    }
}
